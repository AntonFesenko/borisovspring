package quoters;

public class QuoterImpl implements Quoter {
    @InjectRandomInt(min=2, max=7)
    private int repeat;

    private String message;
//для создание через xml нужно обязательно создать сеттер(через рефлекшн вызвать сеттер)иначе упадет
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void sayQuoter() {
        for (int i = 0; i < repeat; i++) {
            System.out.println("message = " + message);
        }
    }
}
