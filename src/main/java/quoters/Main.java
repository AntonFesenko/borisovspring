package quoters;

import org.springframework.context.support.ClassPathXmlApplicationContext;
//*
// Интерфейс org.springframework.context.ApplicationContext представляет контейнер Spring IoC и отвечает за создание,
// настройку и сборку вышеупомянутых компонентов. Контейнер получает инструкции о том, какие объекты нужно создавать,
// настраивать и собирать, считывая метаданные конфигурации. Метаданные конфигурации представлены в XML, Java annotations,
// or Java code. Он позволяет вам выражать объекты, которые составляют ваше приложение, и богатые взаимозависимости между такими объектами.
//
//*****************************************
// Spring ApplicationContext Container
//Так же, как BeanFactory, ApplicationContext загружает бины, связывает их вместе и конфигурирует их определённым образом.
// ApplicationContext обладает дополнительной функциональностью: распознание текстовых сообщений из файлов настройки и отображение событий,
// которые происходят в приложении различными способами. Этот контейнер определяется интерфейсом org.springframework.context.ApplicationContext.
//
//– FileSystemXmlApplicationContext
//Загружает данные о бине из XML файла. При использовании этой реализации в конструкторе необходимо указать полный адрес конфигурационного файла.
//
//– ClassPathXmlApplicationContext
//Этот контейнер также получает данные о бине из XML файла. Но в отличие от FileSystemApplicationContext, в этом случае необходимо указать относительный а
// дрес конфигурационного файла (CLASSPATH).
//
//– WebXmlApplicationContext
//Эта реализация ApplicationContext получает необходимую информацию из веб-приложения.
// */

public class Main {
    public static void main(String[] args) {
        //ClassPathXmlApplicationContext контейнер также получает данные о бине из XML файла
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        // getBean - для загрузки экземпляра и предоставления данных, объявленных этим компонентом
        context.getBean(QuoterImpl.class).sayQuoter();
    }
}
