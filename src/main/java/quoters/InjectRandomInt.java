package quoters;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotations are to be discarded by the compiler.
 * <p>
 * SOURCE, - @Override
 * <p>
 * Annotations are to be recorded in the class file by the compiler
 * but need not be retained by the VM at run time.  This is the default
 * behavior.
 * <p>
 * CLASS,
 * <p>
 * <p>
 * Annotations are to be recorded in the class file by the compiler and
 * retained by the VM at run time, so they may be read reflectively.
 * <p>
 * *@see java.lang.reflect.AnnotatedElement
 * <p>
 * RUNTIME
 */

@Retention(RetentionPolicy.RUNTIME)//усі аннотації для роботи з рефлексією java.lang.reflect.AnnotatedElement
public @interface InjectRandomInt {
    int min();
    int max();
}
